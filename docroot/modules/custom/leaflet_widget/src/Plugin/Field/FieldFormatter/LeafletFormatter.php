<?php

namespace Drupal\leaflet_widget\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Class LeafletFormatter.
 *
 * @package Drupal\leaflet_widget\Plugin\Field\FieldFormatter
 * @FieldFormatter(
 *   id = "leaflet_widget_formatter",
 *   label = @Translation("Leaflet Custom Formatter"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class LeafletFormatter extends FormatterBase {

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta]['#attached']['library'][] = 'leaflet_widget/leaflet_widget';
      $elements[$delta] = [
        '#markup' => '<div id="map-id">ASDASD</div>',
      ];
    }
    return $elements;
  }

}
