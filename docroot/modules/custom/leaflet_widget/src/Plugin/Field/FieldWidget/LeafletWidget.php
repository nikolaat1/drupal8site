<?php

namespace Drupal\leaflet_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\Plugin\Field\FieldWidget\GeofieldBaseWidget;

/**
 * A widget map for leaflet.
 *
 * @FieldWidget(
 *   id = "leaflet_widget",
 *   module = "leaflet_widget",
 *   label = @Translation("Leaflet Map Custom"),
 *   field_types = {
 *      "geofield"
 *   }
 * )
 */
class LeafletWidget extends GeofieldBaseWidget {

  /**
   * Returns the form for a single field widget.
   *
   * Field widget form elements should be based on the passed-in $element, which
   * contains the base form element properties derived from the field
   * configuration.
   *
   * The BaseWidget methods will set the weight, field name and delta values for
   * each form element. If there are multiple values for this field, the
   * formElement() method will be called as many times as needed.
   *
   * Other modules may alter the form element provided by this function using
   * hook_field_widget_form_alter() or
   * hook_field_widget_WIDGET_TYPE_form_alter().
   *
   * The FAPI element callbacks (such as #process, #acelement_validate,
   * #value_callbk, etc.) used by the widget do not have access to the
   * original $field_definition passed to the widget's constructor. Therefore,
   * if any information is needed from that definition by those callbacks, the
   * widget implementing this method, or a
   * hook_field_widget[_WIDGET_TYPE]_form_alter() implementation, must extract
   * the needed properties from the field definition and set them as ad-hoc
   * $element['#custom'] properties, for later use by its element callbacks.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Array of default values for this field.
   * @param int $delta
   *   The order of this item in the array of sub-elements (0, 1, 2, etc.).
   * @param array $element
   *   A form element array containing basic properties for the widget:
   *   - #field_parents: The 'parents' space for the field in the form. Most
   *       widgets can simply overlook this property. This identifies the
   *       location where the field values are placed within
   *       $form_state->getValues(), and is used to access processing
   *       information for the field through the getWidgetState() and
   *       setWidgetState() methods.
   *   - #title: The sanitized element label for the field, ready for output.
   *   - #description: The sanitized element description for the field, ready
   *     for output.
   *   - #required: A Boolean indicating whether the element value is required;
   *     for required multiple value fields, only the first widget's values are
   *     required.
   *   - #delta: The order of this item in the array of sub-elements; see $delta
   *     above.
   * @param array $form
   *   The form structure where widgets are being attached to. This might be a
   *   full form structure, or a sub-element of a larger form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form elements for a single widget for this field.
   *
   * @see hook_field_widget_form_alter()
   * @see hook_field_widget_WIDGET_TYPE_form_alter()
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'leaflet_widget/leaflet-widget';
    $form['#attached']['drupalSettings']['leaflet_widget']['leafletJS']['zoom'] = $this->settings['zoom'];
    $lat = $items->getValue();
    $lat = $lat[0]['lat'];
    $lon = $items->getValue();
    $lon = $lon[0]['lon'];

    $element = [];
    $element['value'] = [
      '#markup' => '<div id="map-id"></div>',
    ];
    $element['lat'] = [
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => !empty($lat) ? $lat : $this->settings['latitude'],
    ];
    $element['lng'] = [
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => !empty($lon) ? $lon : $this->settings['longitude'],
    ];

    return $element;
  }

  /**
   * Default settings which can be chosen for widget gear.
   *
   * @return array
   *   Default settings for form display widget
   */
  public static function defaultSettings() {
    return [
      'zoom' => 5,
      'latitude' => 42,
      'longitude' => 24,
    ] + parent::defaultSettings();
  }

  /**
   * Form display settings fot the widget.
   *
   * @return array|mixed
   *   Return array of $element.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['zoom'] = [
      '#type' => 'number',
      '#title' => t('Map zoom'),
      '#default_value' => $this->getSetting('zoom'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    $element['latitude'] = [
      '#type' => 'number',
      '#title' => t('Latitude default value:'),
      '#default_value' => $this->getSetting('latitude'),
      '#required' => FALSE,
    ];
    $element['longitude'] = [
      '#type' => 'number',
      '#title' => t('Longitude default value:'),
      '#default_value' => $this->getSetting('longitude'),
      '#required' => FALSE,
    ];
    return $element;
  }

  /**
   * Saves the data from form submit to Geofiled base.
   *
   * @return array
   *   Return the array of POINT(lat, lon).
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $lat = $values[0]['lat'];
    $lng = $values[0]['lng'];
    $values[0]['value'] = "POINT (" . $lng . " " . $lat . ")";
    unset($values[0]['lat']);
    unset($values[0]['lng']);
    return $values;
  }

}
