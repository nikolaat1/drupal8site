/**
 * @file
 * JS map.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.leaflet_widget = {
    attach: function (context, settings) {
      let zoom = drupalSettings.leaflet_widget.leafletJS.zoom;

      let theLat = $('#edit-field-map-0-lat').val();
      let theLng = $('#edit-field-map-0-lng').val();
      let mymap = L.map('map-id').setView([theLat, theLng], zoom)
      L.tileLayer(
        'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
        {
          attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox.streets',
          accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
        }).addTo(mymap)

      let pointer = {}

      function onMapClick(e) {

        let coords = (e.latlng)

        if (pointer != undefined) {
          mymap.removeLayer(pointer)
        }
        pointer = L.marker([coords.lat, coords.lng]).addTo(mymap)

        $('#edit-field-map-0-lat').val(coords.lat)
        $('#edit-field-map-0-lng').val(coords.lng)
      }
      mymap.on('click', onMapClick);

      if ((typeof (theLat) !== 'undefined') && (typeof (theLng) !== 'undefined')) {
        pointer = L.marker([theLat, theLng]).addTo(mymap)
      }

    },
  }

}(jQuery, Drupal, drupalSettings))
