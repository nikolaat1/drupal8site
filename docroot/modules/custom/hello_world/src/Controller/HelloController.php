<?php

namespace Drupal\hello_world\Controller;

/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */

/**
 * Class HelloController.
 *
 * @package Drupal\hello_world\Controller.
 */
class HelloController {

  /**
   * Function that return the content.
   *
   * @return array
   *   Function that return the content.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => t('Hello, World!'),
    ];
  }

}
